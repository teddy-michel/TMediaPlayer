/*
Copyright (C) 2012-2021 Teddy Michel

This file is part of TMediaPlayer.

TMediaPlayer is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TMediaPlayer is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TMediaPlayer. If not, see <http://www.gnu.org/licenses/>.
*/

#if 0  // Le site ne fonctionne plus

#include "CLyricWiki.hpp"
#include "CMediaManager.hpp"
#include "CSong.hpp"

#include "htmlcxx/ParserDom.h"

// Qt
#include <QNetworkReply>


/**
 * Constructeur.
 */

CLyricWiki::CLyricWiki(CMediaManager * mediaManager, CSong * song) :
    ILyricFinder { mediaManager, song },
    m_WebCtrl    {}
{
    connect(&m_WebCtrl, SIGNAL(finished(QNetworkReply *)), this, SLOT(onReplyFinished(QNetworkReply *)));

}


void CLyricWiki::search()
{
    QUrl url(QString("https://lyrics.fandom.com/api.php?action=lyrics&artist=%1&song=%2&albumName=%3&fmt=xml&func=getSong").arg(m_song->getArtistName()).arg(m_song->getTitle()).arg(m_song->getAlbumTitle()));
    QNetworkRequest request(url);
    request.setAttribute(QNetworkRequest::RedirectPolicyAttribute, true);
    m_WebCtrl.get(request);
}


void CLyricWiki::onReplyFinished(QNetworkReply * reply)
{
    Q_CHECK_PTR(reply);
    reply->deleteLater();

    if (reply->error() != QNetworkReply::NoError)
    {
        m_mediaManager->logError(tr("HTTP error: %1").arg(reply->error()), __FUNCTION__, __FILE__, __LINE__);
        emit lyricsNotFound();
    }

    QByteArray data = reply->readAll();

    htmlcxx::HTML::ParserDom parser;
    tree<htmlcxx::HTML::Node> dom = parser.parseTree(qPrintable(data));

    for (auto it = dom.begin(); it != dom.end(); ++it)
    {
        if (it->tagName() == "url")
        {
            disconnect(&m_WebCtrl, SIGNAL(finished(QNetworkReply *)), this, SLOT(onReplyFinished(QNetworkReply *)));
            connect(&m_WebCtrl, SIGNAL(finished(QNetworkReply *)), this, SLOT(onReplyFinished2(QNetworkReply *)));

            QUrl url(QString::fromStdString(it.node->first_child->data.text()));
            QNetworkRequest request(url);
            request.setAttribute(QNetworkRequest::RedirectPolicyAttribute, true);
            m_WebCtrl.get(request);

            return;
        }
    }

    emit lyricsNotFound();
}


void CLyricWiki::onReplyFinished2(QNetworkReply * reply)
{
    Q_CHECK_PTR(reply);
    reply->deleteLater();

    if (reply->error() != QNetworkReply::NoError)
    {
        m_mediaManager->logError(tr("HTTP error: %1").arg(reply->error()), __FUNCTION__, __FILE__, __LINE__);
        emit lyricsNotFound();
    }

    QByteArray data = reply->readAll();

    htmlcxx::HTML::ParserDom parser;
    tree<htmlcxx::HTML::Node> dom = parser.parseTree(qPrintable(data));

    for (auto it = dom.begin(); it != dom.end(); ++it)
    {
        if (it->tagName() == "div")
        {
            it->parseAttributes();
            for (const auto& attr : it->attributes())
            {
                if (attr.first == "class" && attr.second == "lyricbox")
                {
                    QString lyrics;
                    for (auto it2 = dom.begin(it); it2 != dom.end(it); ++it2)
                    {
                        lyrics += QString::fromStdString(it2->text());
                    }

                    emit lyricsFound(lyrics);
                    return;
                }
            }
        }
    }

    emit lyricsNotFound();
}

#endif
