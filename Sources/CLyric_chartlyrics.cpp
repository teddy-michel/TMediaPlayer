/*
Copyright (C) 2021 Teddy Michel

This file is part of TMediaPlayer.

TMediaPlayer is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TMediaPlayer is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TMediaPlayer. If not, see <http://www.gnu.org/licenses/>.
*/

#include "CLyric_chartlyrics.hpp"
#include "CMediaManager.hpp"
#include "CSong.hpp"

// Qt
#include <QtXml>
#include <QNetworkReply>


/**
 * Constructeur.
 */

CLyric_chartlyrics::CLyric_chartlyrics(CMediaManager * mediaManager, CSong * song) :
    ILyricFinder { mediaManager, song },
    m_WebCtrl    {}
{
    connect(&m_WebCtrl, SIGNAL(finished(QNetworkReply *)), this, SLOT(onReplyFinished(QNetworkReply *)));

}


void CLyric_chartlyrics::search()
{
    QUrl url(QString("http://api.chartlyrics.com/apiv1.asmx/SearchLyric?artist=%1&song=%2").arg(m_song->getArtistName()).arg(m_song->getTitle()));
    QNetworkRequest request(url);
    request.setAttribute(QNetworkRequest::RedirectPolicyAttribute, true);
    m_WebCtrl.get(request);
}


void CLyric_chartlyrics::onReplyFinished(QNetworkReply * reply)
{
    Q_CHECK_PTR(reply);
    reply->deleteLater();

    if (reply->error() != QNetworkReply::NoError)
    {
        m_mediaManager->logError(tr("HTTP error: %1").arg(reply->error()), __FUNCTION__, __FILE__, __LINE__);
        emit lyricsNotFound();
    }

    QDomDocument xml;
    xml.setContent(reply->readAll());

    QDomElement root = xml.documentElement();
    if (root.tagName() == "ArrayOfSearchLyricResult")
    {
        QDomElement e = root.firstChildElement();
        while (!e.isNull())
        {
            if (e.tagName() == "SearchLyricResult")
            {
                QDomElement e_id = e.firstChildElement("LyricId");
                QDomElement e_checksum = e.firstChildElement("LyricChecksum");
                if (!e_id.isNull() && !e_checksum.isNull())
                {
                    disconnect(&m_WebCtrl, SIGNAL(finished(QNetworkReply *)), this, SLOT(onReplyFinished(QNetworkReply *)));
                    connect(&m_WebCtrl, SIGNAL(finished(QNetworkReply *)), this, SLOT(onReplyFinished2(QNetworkReply *)));

                    QUrl url(QString("http://api.chartlyrics.com/apiv1.asmx/GetLyric?lyricId=%1&lyricCheckSum=%2").arg(e_id.text()).arg(e_checksum.text()));
                    QNetworkRequest request(url);
                    request.setAttribute(QNetworkRequest::RedirectPolicyAttribute, true);
                    m_WebCtrl.get(request);

                    return;
                }
            }
            e = e.nextSiblingElement();
        }
    }

    emit lyricsNotFound();
}


void CLyric_chartlyrics::onReplyFinished2(QNetworkReply * reply)
{
    Q_CHECK_PTR(reply);
    reply->deleteLater();

    if (reply->error() != QNetworkReply::NoError)
    {
        m_mediaManager->logError(tr("HTTP error: %1").arg(reply->error()), __FUNCTION__, __FILE__, __LINE__);
        emit lyricsNotFound();
    }

    QDomDocument xml;
    xml.setContent(reply->readAll());

    QDomElement root = xml.documentElement();
    if (root.tagName() == "GetLyricResult")
    {
        QDomElement e_lyric = root.firstChildElement("Lyric");
        if (!e_lyric.isNull())
        {
            emit lyricsFound(e_lyric.text());
            return;
        }
    }

    emit lyricsNotFound();
}
