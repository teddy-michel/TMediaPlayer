/*
Copyright (C) 2021 Teddy Michel

This file is part of TMediaPlayer.

TMediaPlayer is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TMediaPlayer is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TMediaPlayer. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <QObject>
#include <QString>


class CMediaManager;
class CSong;


/**
 * Classe de base pour rechercher les paroles d'une chanson.
 */

class ILyricFinder : public QObject
{
    Q_OBJECT

public:

    ILyricFinder(CMediaManager * mediaManager, CSong * song);
    virtual ~ILyricFinder() = default;

    inline CSong * getSong() const noexcept;

public slots:

    virtual void search() = 0;

signals:

    void lyricsNotFound();
    void lyricsFound(const QString& lyrics);

protected:

    CMediaManager * m_mediaManager;
    CSong * m_song; ///< Pointeur sur le morceau dont on recherche les paroles.
};


inline CSong * ILyricFinder::getSong() const noexcept
{
    return m_song;
}
