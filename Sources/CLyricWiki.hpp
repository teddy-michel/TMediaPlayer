/*
Copyright (C) 2012-2021 Teddy Michel

This file is part of TMediaPlayer.

TMediaPlayer is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TMediaPlayer is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TMediaPlayer. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#if 0  // Le site ne fonctionne plus

#include <QNetworkAccessManager>
#include "ILyricFinder.hpp"


class CLyricWiki : public ILyricFinder
{
    Q_OBJECT

public:

    CLyricWiki(CMediaManager * mediaManager, CSong * song);
    virtual ~CLyricWiki() = default;

public slots:

    virtual void search();

private slots:

    void onReplyFinished(QNetworkReply * reply);
    void onReplyFinished2(QNetworkReply * reply);

private:

    QNetworkAccessManager m_WebCtrl;
};

#endif
